logstalgia (1.1.4-1) unstable; urgency=medium

  [ Andrew Caudwell ]
  * New upstream release (closes: #1000091)
  * Bump Standards-Version up to 4.6.2
  * Update libfreetype-dev build dependency

  [ Francois Marier ]
  * Update watch file to account for GitHub changes

 -- Andrew Caudwell <acaudwell@gmail.com>  Thu, 26 Jan 2023 10:42:46 +1300

logstalgia (1.1.3-1) unstable; urgency=medium

  * New upstream release
  * Change PCRE build dependency to libpcre2-dev
  * Add libboost-filesystem-dev build dependency
  * Add debhelper-compat build dependency and remove debian/compat
  * Add 'Rules-Requires-Root: no' to debian/control
  * Remove GLM compatibility work around
  * Fix hardening-no-bindnow lintian warning
  * Bump Standards-Version up to 4.6.0.1
  * Bump debian/watch version to 4

 -- Andrew Caudwell <acaudwell@gmail.com>  Wed, 15 Jun 2022 15:08:25 +1200

logstalgia (1.1.0-2) unstable; urgency=medium

  [ Andrew Caudwell ]
  * Fixed build issue with libglm-dev (closes: #888927)
  * Changed priority from extra to optional
  * Update VCS URLs

  [ Francois Marier ]
  * Bump Standards-Version to 4.1.3
  * Bump debhelper compat to 11

 -- Andrew Caudwell <acaudwell@gmail.com>  Tue, 03 Apr 2018 14:52:49 +1200

logstalgia (1.1.0-1) unstable; urgency=medium

  [ Andrew Caudwell ]
  * New upstream version
  * Updated debian/copyright

  [ Francois Marier ]
  * Switch to HTTPS VCS URLs

 -- Andrew Caudwell <acaudwell@gmail.com>  Wed, 11 Oct 2017 13:02:02 +1300

logstalgia (1.0.9-1) unstable; urgency=medium

  * New upstream version
  * Bump Standards-Version to 4.1.1
  * Bump debhelper compatibility to 10

 -- Andrew Caudwell <acaudwell@gmail.com>  Fri, 06 Oct 2017 13:30:32 +1300

logstalgia (1.0.7-1) unstable; urgency=medium

  * New upstream version
  * Homepage URL changed
  * Bump debhelper compatibility to 9

 -- Andrew Caudwell <acaudwell@gmail.com>  Thu, 17 Dec 2015 13:28:09 +1300

logstalgia (1.0.6-1) unstable; urgency=medium

  * New upstream version
  * Made VCS URIs canonical
  * Added dpkg-buildflags
  * Bumped Standards-Version to 3.9.6

 -- Andrew Caudwell <acaudwell@gmail.com>  Thu, 16 Oct 2014 15:38:53 +1300

logstalgia (1.0.5-1) unstable; urgency=low

  * New upstream version
  * Updated debian/copyright
  * Removed old patch
  * Changed watch file to look at Github for releases

 -- Andrew Caudwell <acaudwell@gmail.com>  Fri, 04 Apr 2014 11:13:21 +1300

logstalgia (1.0.3-4) unstable; urgency=low

  * Replaced ttf-freefont dependency with fonts-freefont-ttf (closes: #738233)
  * Bumped Standards-Version to 3.9.5

 -- Andrew Caudwell <acaudwell@gmail.com>  Wed, 12 Feb 2014 13:27:31 +1300

logstalgia (1.0.3-3) unstable; urgency=low

  * Stopped unnecessary checks for JPG/PNG libraries (Closes: #669453)
  * Bump Standards-Version to 3.9.3

 -- Andrew Caudwell <acaudwell@gmail.com>  Tue, 24 Apr 2012 11:17:59 +1200

logstalgia (1.0.3-2) unstable; urgency=low

  [ Andrew Caudwell ]
  * Fixed missing ttf-freefont dependency (Closes: #628802)

  [ Francois Marier ]
  * Bump Standards-Version to 3.9.2

 -- Francois Marier <francois@debian.org>  Thu, 02 Jun 2011 17:49:03 +1200

logstalgia (1.0.3-1) unstable; urgency=low

  * New upstream version
  * Updated package description

 -- Andrew Caudwell <acaudwell@gmail.com>  Wed, 16 Feb 2011 16:43:36 +1300

logstalgia (1.0.0-2) unstable; urgency=low

  * Fix watch file due to change on the Google Code site (see #581622)
  * Include full BSD license in debian/copyright (lintian warning)
  * Bump Standards-Version up to 3.9.1
  * Bump debhelper compatibility to 8

 -- Francois Marier <francois@debian.org>  Wed, 22 Dec 2010 16:34:27 +1300

logstalgia (1.0.0-1) unstable; urgency=low

  * New Upstream Version

 -- Andrew Caudwell <acaudwell@gmail.com>  Wed, 10 Mar 2010 14:46:14 +1300

logstalgia (0.9.8-1) unstable; urgency=low

  * New Upstream Version

 -- Andrew Caudwell <acaudwell@gmail.com>  Tue, 09 Feb 2010 14:31:44 +1300

logstalgia (0.9.7-1) unstable; urgency=low

  * New Upstream Version

 -- Andrew Caudwell <acaudwell@gmail.com>  Fri, 05 Feb 2010 11:47:01 +1300

logstalgia (0.9.6-2) unstable; urgency=low

  [ Andrew Caudwell ]
  * Updated debian/copyright (added missing entry)

  [ Francois Marier ]
  * Switch to 3.0 (quilt) source format

 -- Francois Marier <francois@debian.org>  Tue, 02 Feb 2010 15:05:06 +1300

logstalgia (0.9.6-1) unstable; urgency=low

  [ Andrew Caudwell ]
  * New Upstream Version

  [ Francois Marier ]
  * Bump Standards-Version up to 3.8.4

 -- Francois Marier <francois@debian.org>  Tue, 02 Feb 2010 13:18:34 +1300

logstalgia (0.9.2-2) unstable; urgency=low

  * Add myself as co-maintainer
  * Bump Standards-Version up to 3.8.3
  * Bump debhelper compatibility to 7
  * Fix spelling mistake (apache => Apache) in long description

 -- Francois Marier <francois@debian.org>  Thu, 24 Sep 2009 23:11:15 +1200

logstalgia (0.9.2-1) unstable; urgency=low

  * New Upstream Version (Closes: #502626)

 -- Andrew Caudwell <acaudwell@gmail.com>  Wed, 29 Oct 2008 14:44:52 +1300

logstalgia (0.9.1-2) unstable; urgency=low

  * Fixed the path of example.log in the manual
  * Bump Standards-Version up to 3.8.0 (no changes)

 -- Andrew Caudwell <acaudwell@gmail.com>  Tue, 23 Jul 2008 17:54:00 +1200

logstalgia (0.9.1-1) unstable; urgency=low

  * New Upstream Version
  * Added missing include for strcmp on Sparc (Closes: #479304)
  * Add homepage field to debian/control

 -- Andrew Caudwell <acaudwell@gmail.com>  Tue, 29 Apr 2008 22:46:34 +0000

logstalgia (0.9.0c-1) unstable; urgency=low

  * Initial release (Closes: #474937)

 -- Andrew Caudwell <acaudwell@gmail.com>  Thu, 24 Apr 2008 14:06:00 +1200
